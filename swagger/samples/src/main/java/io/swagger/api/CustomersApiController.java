package io.swagger.api;

import io.swagger.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-26T12:35:09.987-03:00")

@Controller
public class CustomersApiController implements CustomersApi {

    private static final Logger log = LoggerFactory.getLogger(CustomersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public CustomersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> createCustomer(@ApiParam(value = "Customer to create"  )  @Valid @RequestBody Customer customer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteCustomer(@ApiParam(value = "ID of customer that needs to be deleted",required=true) @PathVariable("customerId") String customerId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Customer> getCustomer(@ApiParam(value = "ID of customer that needs to be fetched",required=true) @PathVariable("customerId") String customerId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Customer>(objectMapper.readValue("{  \"crmId\" : \"C645235\",  \"baseUrl\" : \"http://www.platformbuilders.com.br\",  \"name\" : \"Platform Builders\",  \"id\" : \"553fa88c-4511-445c-b33a-ddff58d76886\",  \"login\" : \"contato@platformbuilders.com.br\"}", Customer.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Customer>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Customer>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Customer>> listCustomers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Customer>>(objectMapper.readValue("[ {  \"crmId\" : \"C645235\",  \"baseUrl\" : \"http://www.platformbuilders.com.br\",  \"name\" : \"Platform Builders\",  \"id\" : \"553fa88c-4511-445c-b33a-ddff58d76886\",  \"login\" : \"contato@platformbuilders.com.br\"}, {  \"crmId\" : \"C645235\",  \"baseUrl\" : \"http://www.platformbuilders.com.br\",  \"name\" : \"Platform Builders\",  \"id\" : \"553fa88c-4511-445c-b33a-ddff58d76886\",  \"login\" : \"contato@platformbuilders.com.br\"} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Customer>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Customer>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> modifyCustomer(@ApiParam(value = "ID of customer that needs to be modified",required=true) @PathVariable("customerId") String customerId,@ApiParam(value = "Customer data with one or more fields filled"  )  @Valid @RequestBody Customer customer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> replaceCustomer(@ApiParam(value = "ID of customer that needs to be replaced",required=true) @PathVariable("customerId") String customerId,@ApiParam(value = "Customer to replace"  )  @Valid @RequestBody Customer customer) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
