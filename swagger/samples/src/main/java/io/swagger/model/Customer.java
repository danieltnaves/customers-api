package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Customer
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-05-26T12:35:09.987-03:00")

public class Customer   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("crmId")
  private String crmId = null;

  @JsonProperty("baseUrl")
  private String baseUrl = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("login")
  private String login = null;

  public Customer id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Internal Customer ID, uniquely identifying this customer in the world. 
   * @return id
  **/
  @ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world. ")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Customer crmId(String crmId) {
    this.crmId = crmId;
    return this;
  }

  /**
   * Customer ID in the CRM. 
   * @return crmId
  **/
  @ApiModelProperty(example = "C645235", value = "Customer ID in the CRM. ")


  public String getCrmId() {
    return crmId;
  }

  public void setCrmId(String crmId) {
    this.crmId = crmId;
  }

  public Customer baseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
    return this;
  }

  /**
   * Base URL of the customer container. 
   * @return baseUrl
  **/
  @ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container. ")


  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public Customer name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Customer name 
   * @return name
  **/
  @ApiModelProperty(example = "Platform Builders", value = "Customer name ")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Customer login(String login) {
    this.login = login;
    return this;
  }

  /**
   * Admin login 
   * @return login
  **/
  @ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login ")


  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(this.id, customer.id) &&
        Objects.equals(this.crmId, customer.crmId) &&
        Objects.equals(this.baseUrl, customer.baseUrl) &&
        Objects.equals(this.name, customer.name) &&
        Objects.equals(this.login, customer.login);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, crmId, baseUrl, name, login);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    crmId: ").append(toIndentedString(crmId)).append("\n");
    sb.append("    baseUrl: ").append(toIndentedString(baseUrl)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

