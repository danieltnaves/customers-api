## Desafio Spring Boot + Mongo DB


**Visão geral:**

A solução foi construída utilizando um recurso adicional para segurança das APIs, que faz uso do projeto Spring Security
e Spring OAuth2 e é um padrão de mercado. Além disso, utiliza tokens no formato JWT, onde é possível colocar informações
adicionais não sensíveis.

Existem duas roles de usuários configuradas que são SYSTEM e CRM.

Os usuários que possuem a role SYSTEM podem acessar operações com os verbos HTTP POST, PUT, PATCH, DELETE e GET.

![picture](images/system.png)

Os usuários que possuem a role USER podem acessar operações com o verbo HTTP GET.

![picture](images/user.png)

**Utilização da aplicação via Swagger UI:**

Para acessar a operações disponíveis é necessário gerar um token JWT de acesso realizando os seguintes passos:

1 - Acessar: http://localhost:8080/api/swagger-ui.html

![picture](images/ui.png)

2 - No canto superior direito clicar em "Authorize"

![picture](images/authorize.png)

3 - Preencher os campos com os seguintes dados:

Usuários

Role SYSTEM

```
User: SYSTEM_BUILDER
Password: 123
```

Role CUSTOMER
```
User: CUSTOMER_BUILDER
Password: 123
```

Client authentication

```
Type: Basic Auth
ClientId: APP_CLIENT
ClientSecret: 549d93b9-6fab-4c08-a2b5-5925c9bdba67
```

Scopes

```
Selecionar read/write
```

A tela deverá ficar da seguinte forma:

![picture](images/authorizations.png)


A partir desse momento já será possível realizar as operações permitidas de acordo com as roles do usuário. Deverá aparecer
uma exclamação no canto direito da operação com um ícone azul.

![picture](images/azul.png)

**Build e execução:**

Para subir a aplicação utilizar o comando:

```
mvn spring-boot:run
```

**MongoDB:**

Nome do documento

```
daniel_naves_customer
```

**Testes:**

Além dos testes unitários também foram criados testes integrados que executam todas operações disponíveis para o resource
de Customer. Esses testes cria, altera e deleta customers da base de dados para garantir o funcionamento da API. Além disso,
também são executadas validações de estrutura de dados recebidas, http status code de resposta e se a roles e acesso por
token estão funcionando corretamente.

Comando para executar os testes integrados:

```
mvn clean test -Dspring.profiles.active=integration-tests
```

**Postman:**

Alternativamente também foi exportada uma collection no Postman para acessar as operações da API, essa collection está
na raiz do diretório.

```
CustomerAPI.json
```


