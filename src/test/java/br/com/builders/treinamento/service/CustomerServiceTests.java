package br.com.builders.treinamento.service;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.ConflictException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.impl.CustomerServiceImpl;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CustomerServiceTests {

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository customerRepository;

    private Customer customer;
    private Customer newCustomer;
    private String expectedId;

    @Before
    public void setup() {
        expectedId = "5b0b31f027347ea74dc0fcf0";
        customer = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        newCustomer = new Customer("XXX553fa88c-4511-445c-b33a-ddff58d76886", "XXXXC645235",
                "XXXXhttp://www.platformbuilders.com.br", "XXXXPlatform Builders", "XXXXcontato@platformbuilders.com.br");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNullCustomerThrowsIllegelException() throws ConflictException {
        customerService.createCustomer(null);
    }

    @Test(expected = ConflictException.class)
    public void testIfDuplicateLoginThrowsConflictException() throws ConflictException {
        when(customerRepository.findByLogin(any(String.class))).thenReturn(customer);
        customerService.createCustomer(customer);
    }

    @Test
    public void testIfCustomerWasCreatedWithSuccess() throws ConflictException {
        customer.setId(expectedId);

        when(customerRepository.findByLogin(any(String.class))).thenReturn(null);
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        Customer createdCustomer = customerService.createCustomer(this.customer);
        assertEquals(expectedId, createdCustomer.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNullReplaceCustomerThrowsIllegalArgumentException() throws NotFoundException {
        customerService.replaceCustomer(expectedId, null);
    }

    @Test(expected = NotFoundException.class)
    public void testIfNotFoundIdThrowsNotFoundException() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(false, true);
        customerService.replaceCustomer(expectedId, customer);
    }

    @Test(expected = NotFoundException.class)
    public void testIfNotFoundCustomerIdThrowsNotFoundException() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(true, false);
        customerService.replaceCustomer(expectedId, customer);
    }

    @Test
    public void testIfCustomerWasReplacedWithSuccessByCustomerId() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(true, true);
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);
        Customer replacedCustomer = customerService.replaceCustomer(this.customer.getId(), this.customer);
        assertEquals(customer.getName(), replacedCustomer.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfNullModifyCustomerThrowsIllegelException() throws IllegalArgumentException, NotFoundException {
        customerService.modifyCustomer(expectedId,null);
    }

    @Test(expected = NotFoundException.class)
    public void testIfModifyCustomerThrowsNotFoundExceptionWhenIdNotFound() throws IllegalArgumentException, NotFoundException{
        when(customerRepository.exists(any(String.class))).thenReturn(false);
        customerService.modifyCustomer(expectedId, customer);
    }

    @Test
    public void testIfCustomerWasModifiedWithSuccess() throws NotFoundException {
        String expectedCrmId = "XXXXC645235";
        Customer savedCustomer = new Customer();
        savedCustomer.setCrmId(expectedCrmId);

        when(customerRepository.findOne(any(String.class))).thenReturn(customer);
        when(customerRepository.exists(any(String.class))).thenReturn(true);
        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        Customer newCustomer = customerService.modifyCustomer(expectedId, savedCustomer);
        assertEquals(expectedCrmId, newCustomer.getCrmId());
    }

    @Test
    public void testIfEmptyCustomerDataConsiderOriginalObject() throws NotFoundException {

        Customer savedCustomer = new Customer();

        when(customerRepository.findOne(any(String.class))).thenReturn(customer);
        when(customerRepository.exists(any(String.class))).thenReturn(true);
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        Customer newCustomer = customerService.modifyCustomer(expectedId, savedCustomer);
        assertEquals("553fa88c-4511-445c-b33a-ddff58d76886", newCustomer.getId());
        assertEquals("C645235", newCustomer.getCrmId());
        assertEquals("http://www.platformbuilders.com.br", newCustomer.getBaseUrl());
        assertEquals("Platform Builders", newCustomer.getName());
        assertEquals("contato@platformbuilders.com.br", newCustomer.getLogin());
    }

    @Test
    public void testIfFullCustomerDataConsiderOriginalObject() throws NotFoundException {

        when(customerRepository.findOne(any(String.class))).thenReturn(newCustomer);
        when(customerRepository.exists(any(String.class))).thenReturn(true);
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        Customer modifiedCustomer = customerService.modifyCustomer(expectedId, customer);
        assertEquals("553fa88c-4511-445c-b33a-ddff58d76886", modifiedCustomer.getId());
        assertEquals("C645235", modifiedCustomer.getCrmId());
        assertEquals("http://www.platformbuilders.com.br", modifiedCustomer.getBaseUrl());
        assertEquals("Platform Builders", modifiedCustomer.getName());
        assertEquals("contato@platformbuilders.com.br", modifiedCustomer.getLogin());
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteNotFoundCustomer() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(false);
        customerService.deleteCustomer(expectedId);
    }

    @Test
    public void testDeleteDeleteCustomer() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(true);
        doNothing().when(customerRepository).delete(any(String.class));
        customerService.deleteCustomer(expectedId);
    }

    @Test
    public void testGetCustomer() throws NotFoundException {
        when(customerRepository.exists(any(String.class))).thenReturn(true);
        when(customerRepository.findOne(any(String.class))).thenReturn(customer);
        Customer getCustomer = customerService.getCustomer(expectedId);
        assertEquals("553fa88c-4511-445c-b33a-ddff58d76886", getCustomer.getId());
        assertEquals("C645235", getCustomer.getCrmId());
        assertEquals("http://www.platformbuilders.com.br", getCustomer.getBaseUrl());
        assertEquals("Platform Builders", getCustomer.getName());
        assertEquals("contato@platformbuilders.com.br", getCustomer.getLogin());
    }

    @Test
    public void testListCustomers() throws NotFoundException {
        List<Customer> customerList = Lists.newArrayList(customer, customer, customer);
        when(customerRepository.findAll()).thenReturn(customerList);
        List<Customer> customers = customerService.listCustomers();
        assertEquals(customerList.size(), customers.size());
    }

}
