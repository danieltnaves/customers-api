package br.com.builders.treinamento.enumeration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class RolesEnumTests {

    @Test
    public void testIfGetCodeRetunValidValue() {
        assertEquals("CUSTOMER", RolesEnum.get("CUSTOMER").name());
        assertEquals("SYSTEM", RolesEnum.get("SYSTEM").name());
    }

    @Test(expected = IllegalStateException.class)
    public void testIfGetCodeRetunInvalidValue() {
        RolesEnum.get("INVALID_CODE");
    }

}