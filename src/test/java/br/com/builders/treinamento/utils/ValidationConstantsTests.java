package br.com.builders.treinamento.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ValidationConstantsTests {

    @Test
    public void testEmailPattern() {
        assertTrue("builders@platform.com.br".matches(ValidationConstants.PATTERN_VALIDATION_EMAIL));
        assertTrue(!"buildersplatform.com.br".matches(ValidationConstants.PATTERN_VALIDATION_EMAIL));
        assertTrue(!"buildersplatform.com.2br".matches(ValidationConstants.PATTERN_VALIDATION_EMAIL));
    }
}
