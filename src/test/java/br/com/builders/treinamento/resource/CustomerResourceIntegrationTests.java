package br.com.builders.treinamento.resource;

import br.com.builders.treinamento.TreinamentoApplication;
import br.com.builders.treinamento.domain.Customer;

import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

import br.com.builders.treinamento.service.CustomerService;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = TreinamentoApplication.class)
@IfProfileValue(name = "spring.profiles.active", value = "integration-tests")
public class CustomerResourceIntegrationTests {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private Customer customer;

    private MockMvc mvc;

    @Value("${security.oauth2.resource.client.id}")
    private String CLIENT_ID;

    @Value("${security.oauth2.resource.client.secret}")
    private String CLIENT_SECRET;

    private String CONTENT_TYPE = "application/json;charset=UTF-8";

    public static final String CUSTOMERS = "/customers";

    public static final String AUTHORIZATION = "Authorization";

    public static final String BEARER = "Bearer ";

    public static final String LOCATION = "location";

    private String CUSTOMER_TOKEN;

    private String SYSTEM_TOKEN;

    private ObjectMapper mapper;

    @Before
    public void setup() throws Exception {
        customer = new Customer(UUID.randomUUID().toString(), "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders Test User", UUID.randomUUID().toString() + "contato@platformbuilders.com.br");
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
        CUSTOMER_TOKEN = obtainAccessToken("CUSTOMER_BUILDER", "123");
        SYSTEM_TOKEN = obtainAccessToken("SYSTEM_BUILDER", "123");
        mapper = new ObjectMapper();
    }

    private String obtainAccessToken(String username, String password) throws Exception {
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("client_id", CLIENT_ID);
        params.add("username", username);
        params.add("password", password);

        ResultActions result = mvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE));

        String resultString = result.andReturn().getResponse().getContentAsString();
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Test
    public void testForbiddenAccessForCustomerRole() throws Exception {

        mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isForbidden());

        mvc.perform(delete(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isForbidden());

        mvc.perform(patch(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isForbidden());

        mvc.perform(put(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isForbidden());

    }

    @Test
    public void testUnauthorizedAccessWithoutToken() throws Exception {

        mvc.perform(get(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        mvc.perform(get("/customers/123")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        mvc.perform(delete(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        mvc.perform(patch(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

        mvc.perform(put(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void testCreateCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());
        mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated());

        mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isConflict());

    }

    @Test
    public void testGetCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());
        MvcResult postResult = mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated()).andReturn();

        String createdCustomerLocation = postResult.getResponse().getHeader(LOCATION);

        mvc.perform(get(createdCustomerLocation)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(customer.getId())))
                .andExpect(jsonPath("$.crmId", is(customer.getCrmId())))
                .andExpect(jsonPath("$.baseUrl", is(customer.getBaseUrl())))
                .andExpect(jsonPath("$.name", is(customer.getName())))
                .andExpect(jsonPath("$.login", is(customer.getLogin())));

    }

    @Test
    public void testDeleteCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());
        MvcResult postResult = mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated()).andReturn();

        String createdCustomerLocation = postResult.getResponse().getHeader(LOCATION);

        mvc.perform(delete(createdCustomerLocation)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN))
                .andExpect(status().isOk());
    }

    @Test
    public void testReplaceWithAllRequiredFieldsCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());

        MvcResult postResult = mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated()).andReturn();

        String createdCustomerLocation = postResult.getResponse().getHeader(LOCATION);

        String newName = "Builders Put New Name";
        customer.setName(newName);

        mvc.perform(put(createdCustomerLocation)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isOk());
    }

    @Test
    public void testReplaceWithNullRequiredFieldCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());

        MvcResult postResult = mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated()).andReturn();

        String createdCustomerLocation = postResult.getResponse().getHeader(LOCATION);

        customer.setName(null);

        mvc.perform(put(createdCustomerLocation)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testModifyOnlyOneFieldForCustomer() throws Exception {
        customer.setLogin(UUID.randomUUID().toString() + customer.getLogin());

        MvcResult postResult = mvc.perform(post(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated()).andReturn();

        String createdCustomerLocation = postResult.getResponse().getHeader(LOCATION);

        Customer modifiedCustomer = new Customer();
        modifiedCustomer.setName("New Patched User Name");

        mvc.perform(patch(createdCustomerLocation)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + SYSTEM_TOKEN)
                .content(mapper.writeValueAsString(modifiedCustomer)))
                .andExpect(status().isOk());
    }

    @Test
    public void testListUsers() throws Exception {
        mvc.perform(get(CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTHORIZATION, BEARER + CUSTOMER_TOKEN))
                .andExpect(status().isOk());
    }
}
