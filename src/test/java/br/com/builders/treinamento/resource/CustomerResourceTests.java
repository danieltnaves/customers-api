package br.com.builders.treinamento.resource;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.ConflictException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.resources.CustomerResource;
import br.com.builders.treinamento.service.CustomerService;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CustomerResourceTests {

    @InjectMocks
    private CustomerResource customerResource;

    @Mock
    private CustomerService customerService;


    private Customer customer;
    private String expectedId;
    private CustomerRequest customerRequest;

    @Before
    public void setup() {
        expectedId = "5b0b31f027347ea74dc0fcf0";
        customer = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        customerRequest = new CustomerRequest("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
    }

    @Test
    public void testIfCustomerWasCreated() throws ConflictException {
        when(customerService.createCustomer(any(Customer.class))).thenReturn(customer);
        ResponseEntity<Void> customerResponse = customerResource.createCustomer(customerRequest);
        assertEquals(HttpStatus.CREATED, customerResponse.getStatusCode());
    }

    @Test
    public void testIfCustomerWasDeleted() throws NotFoundException {
        doNothing().when(customerService).deleteCustomer(any(String.class));
        ResponseEntity<Void> customerResponse = customerResource.deleteCustomer(expectedId);
        assertEquals(HttpStatus.OK, customerResponse.getStatusCode());
    }

    @Test
    public void testGetCustomer() throws NotFoundException {
        when(customerService.getCustomer(any(String.class))).thenReturn(customer);
        ResponseEntity<CustomerResponse> customerResponse = customerResource.getCustomer(expectedId);
        assertEquals(HttpStatus.OK, customerResponse.getStatusCode());
    }

    @Test
    public void testListCustomers() {
        when(customerService.listCustomers()).thenReturn(Lists.newArrayList(customer, customer, customer));
        ResponseEntity<List<CustomerResponse>> customerResponse = customerResource.listCustomers();
        assertEquals(HttpStatus.OK, customerResponse.getStatusCode());
    }

    @Test
    public void testIfCustomerWasModified() throws NotFoundException {
        when(customerService.modifyCustomer(any(String.class), any(Customer.class))).thenReturn(customer);
        ResponseEntity<Void> customerResponse = customerResource.modifyCustomer(expectedId, customerRequest);
        assertEquals(HttpStatus.OK, customerResponse.getStatusCode());
    }

    @Test
    public void testIfCustomerWasReplaced() throws NotFoundException {
        when(customerService.replaceCustomer(any(String.class), any(Customer.class))).thenReturn(customer);
        ResponseEntity<Void> customerResponse = customerResource.replaceCustomer(expectedId, customerRequest);
        assertEquals(HttpStatus.OK, customerResponse.getStatusCode());
    }

}
