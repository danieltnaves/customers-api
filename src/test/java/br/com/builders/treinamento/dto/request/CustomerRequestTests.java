package br.com.builders.treinamento.dto.request;

import br.com.builders.treinamento.dto.groups.ModifyCustomerGroup;
import br.com.builders.treinamento.dto.groups.NewCustomerGroup;
import br.com.builders.treinamento.dto.groups.ReplaceCustomerGroup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CustomerRequestTests {

    private CustomerRequest customerRequest;
    private Validator validator;

    @Before
    public void setup() {
        customerRequest = new CustomerRequest("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testIfNullValuesWasValidatedForNewCustomerGroup() {
        Set<ConstraintViolation<CustomerRequest>> violations = validator.validate(new CustomerRequest(), NewCustomerGroup.class);
        assertEquals(3, violations.size());
    }

    @Test
    public void testIfNullValuesWasValidatedForReplaceCustomerGroup() {
        Set<ConstraintViolation<CustomerRequest>> violations = validator.validate(new CustomerRequest(), ReplaceCustomerGroup.class);
        assertEquals(5, violations.size());
    }

    @Test
    public void testIfNullValuesWasValidatedForModifyCustomerGroup() {
        Set<ConstraintViolation<CustomerRequest>> violations = validator.validate(new CustomerRequest(), ModifyCustomerGroup.class);
        assertEquals(0, violations.size());
    }

    @Test
    public void testIfBigStringValuesWasValidated() {
        char[] data = new char[350];

        String crmId = new String(data);
        String name = new String(data);
        String login = new String(data);

        customerRequest.setCrmId(crmId);
        customerRequest.setName(name);
        customerRequest.setLogin(login);

        Set<ConstraintViolation<CustomerRequest>> violations = validator.validate(customerRequest, ModifyCustomerGroup.class);
        assertEquals(4, violations.size());
    }

    @Test
    public void testIfEmailPatternWasValidated() {
        customerRequest.setLogin("xxxxxbbbb.com.br");
        Set<ConstraintViolation<CustomerRequest>> violations = validator.validate(customerRequest, ModifyCustomerGroup.class);
        assertEquals(1, violations.size());
    }

}
