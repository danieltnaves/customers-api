package br.com.builders.treinamento.dto.mapper;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CustomerMapperTests {

    private CustomerRequest customerRequest;
    private Customer customer;

    @Before
    public void setup() {
        customerRequest = new CustomerRequest("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
        customer = new Customer("553fa88c-4511-445c-b33a-ddff58d76886", "C645235",
                "http://www.platformbuilders.com.br", "Platform Builders", "contato@platformbuilders.com.br");
    }

    @Test
    public void testCustomerRequestToCustomerMapper() {
        customer = CustomerMapper.INSTANCE.customerRequestToCustomer(customerRequest);
        assertEquals(customer.getId(), customerRequest.getId());
        assertEquals(customer.getCrmId(), customerRequest.getCrmId());
        assertEquals(customer.getBaseUrl(), customerRequest.getBaseUrl());
        assertEquals(customer.getName(), customerRequest.getName());
        assertEquals(customer.getLogin(), customerRequest.getLogin());
    }

    @Test
    public void testCustomerToCustomerResponse() {
        CustomerResponse customerResponse = CustomerMapper.INSTANCE.customerToCustomerResponse(customer);
        assertEquals(customerResponse.getId(), customer.getId());
        assertEquals(customerResponse.getCrmId(), customer.getCrmId());
        assertEquals(customerResponse.getBaseUrl(), customer.getBaseUrl());
        assertEquals(customerResponse.getName(), customer.getName());
        assertEquals(customerResponse.getLogin(), customer.getLogin());
    }

    @Test
    public void testCustomersToCustomersResponse() {
        List<CustomerResponse> customerResponseList = CustomerMapper.INSTANCE.customersToCustomersResponse(Lists.newArrayList(customer, customer, customer));
        assertEquals(3, customerResponseList.size());
    }
}
