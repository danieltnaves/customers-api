package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ConflictException extends ErrorMessageAPIException {

  private static final long serialVersionUID = 1L;

  public ConflictException(final String errorCode) {
    super(HttpStatus.CONFLICT, errorCode);
  }

  public ConflictException(final ErrorMessage erros) {
    super(HttpStatus.CONFLICT, erros);
  }

  public ConflictException(final List<ErrorMessage> erros) {
    super(HttpStatus.CONFLICT, erros);
  }

}
