/*
* Copyright 2018 Builders
*************************************************************
*Nome     : NotFoundException.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;

import java.util.List;

public class NotFoundException extends ErrorMessageAPIException {

	private static final long serialVersionUID = 1L;

	public NotFoundException(final String errorCode) {
		super(HttpStatus.NOT_FOUND, errorCode);
	}

	public NotFoundException(final ErrorMessage erros) {
		super(HttpStatus.NOT_FOUND, erros);
	}

	public NotFoundException(final List<ErrorMessage> erros) {
		super(HttpStatus.NOT_FOUND, erros);
	}

}

