package br.com.builders.treinamento.service;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.ConflictException;
import br.com.builders.treinamento.exception.NotFoundException;

import java.util.List;

public interface CustomerService {

    /**
     * Cria um novo customer na base de dados se o login fornecido não existir na base de dados.
     *
     * @param customer dados do customer
     * @return novo customer criado
     */
    Customer createCustomer(Customer customer) throws ConflictException;

    /**
     * Verifica se um dado customer existe na base de dados, se o customer
     * existir seus dados serão atualizados na base de dados de forma total.
     *
     * @param customerId id do customer a ser atualizado
     * @param customer dados do customer para atualização
     * @return customer atualizado
     */
    Customer replaceCustomer(String customerId, Customer customer) throws NotFoundException;


    /**
     * Verifica se um dado customer existe na base de dados, se o customer
     * existir seus dados serão atualizados de forma parcial.
     *
     * @param customerId id do customer a ser atualizado
     * @param customer dados do customer para atualização
     * @return ustomer modificado
     */
    Customer modifyCustomer(String customerId, Customer customer) throws NotFoundException;

    /**
     * Deleta um customer a partir de um dado ID
     *
     * @param customerId id do customer a ser deletado
     * @throws NotFoundException
     */
    void deleteCustomer(String customerId) throws NotFoundException;

    /**
     * Retorna um customer com base no ID informado no parâmetro
     *
     * @param customerId
     * @return dados do customer
     * @throws NotFoundException
     */
    Customer getCustomer(String customerId) throws NotFoundException;


    /**
     * Lista os customers cadastrados na base
     *
     * @return lista de customers cadastrados
     */
    List<Customer> listCustomers();


}
