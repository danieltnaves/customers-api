package br.com.builders.treinamento.service.impl;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.exception.*;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer createCustomer(Customer customer) throws ConflictException {

        log.info("m=createCustomer, Customer request data {}", customer);
        verifyEmptyCustomer(customer);

        /*
         * Verify if user already exists in database.
         */
        if (!StringUtils.isEmpty(customerRepository.findByLogin(customer.getLogin()))) {
            throw new ConflictException(new ErrorMessage(ErrorCodes.CUSTOMER_CONFLICT));
        }

        Customer createdCustomer = customerRepository.save(customer);
        log.info("m=createCustomer, New Customer created {}", createdCustomer);

        return createdCustomer;
    }

    @Override
    public Customer replaceCustomer(String customerId, Customer customer) throws NotFoundException {

        log.info("m=replaceCustomer, Customer request ID {}, customer data {}", customerId, customer);
        verifyEmptyCustomer(customer);

        /*
         * Verifica se o ID do usuário existe
         */
        if (!customerRepository.exists(customerId) || !customerRepository.exists(customer.getId())) {
            throw new NotFoundException(ErrorCodes.CUSTOMER_NOT_FOUND);
        }

        Customer replacedCustomer = customerRepository.save(customer);
        log.info("m=replaceCustomer, Customer replaced {}", replacedCustomer);
        return replacedCustomer;
    }

    @Override
    public Customer modifyCustomer(String customerId, Customer customer) throws NotFoundException {
        log.info("m=modifyCustomer, Customer request ID {}, customer data {}", customerId, customer);

        verifyEmptyCustomer(customer);
        verifyCustomerId(customerId);

        /*
         * Verifica se algum dado foi enviado e realiza o update dos campos.
         */
        Customer persistedCustomer = customerRepository.findOne(customerId);
        persistedCustomer.setCrmId(customer.getCrmId() != null ? customer.getCrmId() : persistedCustomer.getCrmId());
        persistedCustomer.setBaseUrl(customer.getBaseUrl() != null ? customer.getBaseUrl() : persistedCustomer.getBaseUrl());
        persistedCustomer.setName(customer.getName() != null ? customer.getName() : persistedCustomer.getName());
        persistedCustomer.setLogin(customer.getLogin() != null ? customer.getLogin() : persistedCustomer.getLogin());


        Customer modifiedCustomer = customerRepository.save(persistedCustomer);
        log.info("m=replaceCustomer, Customer modified {}", modifiedCustomer);
        return modifiedCustomer;
    }

    @Override
    public void deleteCustomer(String customerId) throws NotFoundException {
        log.info("m=deleteCustomer, Trying to delete customer ID {}", customerId);
        verifyCustomerId(customerId);
        customerRepository.delete(customerId);
        log.info("m=deleteCustomer, Customer deleted {}", customerId);

    }

    @Override
    public Customer getCustomer(String customerId) throws NotFoundException {
        verifyCustomerId(customerId);
        Customer customer = customerRepository.findOne(customerId);
        log.info("m=getCustomer, Customer data {}", customer);
        return customer;
    }

    @Override
    public List<Customer> listCustomers() {
        return customerRepository.findAll();
    }

    private void verifyEmptyCustomer(Customer customer) {
        if (customer == null) {
            throw new IllegalArgumentException(ErrorCodes.ILLEGAL_ARGUMENT);
        }
    }

    private void verifyCustomerId(String customerId) throws NotFoundException {
        /*
         * Verifica se o ID do usuário existe
         */
        if (!customerRepository.exists(customerId)) {
            throw new NotFoundException(ErrorCodes.CUSTOMER_NOT_FOUND);
        }
    }
}
