package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.groups.ModifyCustomerGroup;
import br.com.builders.treinamento.dto.groups.NewCustomerGroup;
import br.com.builders.treinamento.dto.groups.ReplaceCustomerGroup;
import br.com.builders.treinamento.dto.mapper.CustomerMapper;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.ConflictException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.service.CustomerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@Api(value = "customers")
public class CustomerResource {

    @Autowired
    private CustomerService customerService;

    @Value("${server.port}")
    private String serverPort;

    @Value("${server.contextPath}")
    private String contextPath;

    @Value("${server.hostname}")
    private String hostName;

    /**
     * Cria um novo customer com base na regras de validação setada na classe NewCustomerRequest.
     *
     * Retorna o http status code 201 caso o customer seja criado com sucesso, 400 para objetos inválidos passados
     * como parâmetro e 409 caso o login fornecido já exista.
     *
     * @param customerRequest
     * @return
     * @throws BadRequestAPIException
     * @throws ConflictException
     */
    @ApiOperation(value = "Creates new customer", nickname = "createCustomer", notes = "Creates new customer", tags={ "system" })
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Item created"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 409, message = "An existing item(login) already exists") })
    @RequestMapping(value = "/customers", produces = { "application/json" }, consumes = { "application/json" }, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createCustomer(@ApiParam(value = "Customer to create") @Validated({NewCustomerGroup.class}) @RequestBody CustomerRequest customerRequest)
            throws ConflictException {
        Customer customer = customerService.createCustomer(CustomerMapper.INSTANCE.customerRequestToCustomer(customerRequest));
        HttpHeaders headers = new HttpHeaders();
        String endpoint = "/customers/" + customer.getId();
        headers.add("Location", endpoint); //created customer location
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    /**
     * Deleta um customer a partir do ID, caso o ID não exista será retornado um http status code 404. Se o usuário
     * for deletado um status code 200 será retornado.
     *
     * @param customerId
     * @return
     * @throws NotFoundException
     */
    @ApiOperation(value = "Deletes a customer", nickname = "deleteCustomer", notes = "Deletes a customer", tags={ "system" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item deleted"),
            @ApiResponse(code = 404, message = "The customer is not found") })
    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCustomer(@ApiParam(value = "ID of customer that needs to be deleted",required=true)
                                        @PathVariable("customerId") String customerId) throws NotFoundException {
        customerService.deleteCustomer(customerId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Retornar os dados de um customer a partir de um ID. Caso não seja encontrado um http status code 404 será
     * retornado.
     *
     * @param customerId
     * @return customer dados do customer
     * @throws NotFoundException
     */
    @ApiOperation(value = "List all data about a customer", nickname = "getCustomer", notes = "List all data about a customer",
            response = CustomerResponse.class, tags={ "customers" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "A single customer", response = CustomerResponse.class),
            @ApiResponse(code = 404, message = "Customer with this ID not found") })
    @RequestMapping(value = "/customers/{customerId}", produces = { "application/json" }, method = RequestMethod.GET)
    public ResponseEntity<CustomerResponse> getCustomer(@ApiParam(value = "ID of customer that needs to be fetched",required=true)
                                         @PathVariable("customerId") String customerId) throws NotFoundException {
        Customer customer = customerService.getCustomer(customerId);
        return new ResponseEntity<>(CustomerMapper.INSTANCE.customerToCustomerResponse(customer), HttpStatus.OK);
    }

    /**
     * Lista os clientes cadastrados e retorna um http status code 200.
     *
     * @return
     */
    @ApiOperation(value = "List all customers / containers (with essential data, enough for displaying a table)",
            nickname = "listCustomers", notes = "List all customers / containers (with essential data, enough for displaying a table)", response = CustomerResponse.class, responseContainer = "List",
            tags={ "customers", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Search results matching criteria", response = CustomerResponse.class, responseContainer = "List") })
    @RequestMapping(value = "/customers", produces = { "application/json" }, method = RequestMethod.GET)
    public ResponseEntity<List<CustomerResponse>> listCustomers() {
        return new ResponseEntity<>(CustomerMapper.INSTANCE.customersToCustomersResponse(customerService.listCustomers()), HttpStatus.OK);
    }

    /**
     * Substitui o conjunto de dados fornecidos como parâmetro para o customerId informado. Essa operação tem a capacidade
     * de realizar a substituição dos dados de forma parcial evitando o overload de informações na entrada.
     *
     * Caso o customer seja modificado com sucesso um http status code 200 será retornado, em caso de objetos inválidos
     * um 400 será retornado e se os IDs informados não existirem na base de dados um 404 será retornado.
     *
     * @param customerId
     * @param customerRequest
     * @return
     * @throws NotFoundException
     */
    @ApiOperation(value = "Modifies a customer", nickname = "modifyCustomer", notes = "Modifies a customer", tags={ "system" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item modified"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
    @RequestMapping(value = "/customers/{customerId}", produces = { "application/json" }, consumes = { "application/json" }, method = RequestMethod.PATCH)
    public ResponseEntity<Void> modifyCustomer(@ApiParam(value = "ID of customer that needs to be modified", required=true)
                                        @PathVariable("customerId") String customerId,
                                        @ApiParam(value = "Customer data with one or more fields filled")
                                        @Validated({ModifyCustomerGroup.class}) @RequestBody CustomerRequest customerRequest) throws NotFoundException {
        customerService.modifyCustomer(customerId, CustomerMapper.INSTANCE.customerRequestToCustomer(customerRequest));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Substitui um usuário existente com base nos dados fornecidos como parâmetro. Caso a substituição ocorra com sucesso
     * o http status code 200 será retornado. Se um objeto inválido for fornecido será retornado o código 400. Caso
     * nenhum dos IDs seja encontrado na base de dados o código 404 será retornado.
     *
     * @param customerId id do customer a ser substituído
     * @param customerRequest dados do customer a ser substituído.
     * @return
     * @throws NotFoundException
     */
    @ApiOperation(value = "Replaces a customer", nickname = "replaceCustomer", notes = "Replaces a customer", tags={ "system" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Item replaced"),
            @ApiResponse(code = 400, message = "Invalid input, object invalid"),
            @ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
    @RequestMapping(value = "/customers/{customerId}", produces = { "application/json" }, consumes = { "application/json" }, method = RequestMethod.PUT)
    public ResponseEntity<Void> replaceCustomer(@ApiParam(value = "ID of customer that needs to be replaced", required=true)
                                         @PathVariable("customerId") String customerId,
                                         @ApiParam(value = "Customer to replace")
                                         @Validated({ReplaceCustomerGroup.class}) @RequestBody CustomerRequest customerRequest) throws NotFoundException {
        customerService.replaceCustomer(customerId, CustomerMapper.INSTANCE.customerRequestToCustomer(customerRequest));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
