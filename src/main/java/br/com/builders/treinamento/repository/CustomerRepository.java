package br.com.builders.treinamento.repository;

import br.com.builders.treinamento.domain.Customer;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;

@Lazy
/**
 * Interface resposável em realizar operações de banco para as entidades Customer.
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {

    /**
     * Busca um customer a partir de um dado login.
     *
     * @param login e-mail do customer
     * @return customer relacionado com o login fornecido.
     */
    Customer findByLogin(String login);

}


