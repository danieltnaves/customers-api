package br.com.builders.treinamento.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "daniel_naves_customer")
public class Customer {

    @Id
    private String id;
    private String crmId;
    private String baseUrl;
    private String name;
    private String login;

}
