package br.com.builders.treinamento.config;

import br.com.builders.treinamento.enumeration.RolesEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * Classe responsável em proteger os resources da aplicação e fazer a verifição dos tokens JWT recebidos na requisição
 * via Bearer Auth.
 */
@RestController
@EnableResourceServer
@Configuration
public class ResourceProviderConfiguration extends ResourceServerConfigurerAdapter {

	public static final String CUSTOMERS = "/customers/**";

	@Value("${security.oauth2.resource.id}")
	private String resourceId;

	@Value("${security.oauth2.resource.jwt.sign.key}")
	private String signKey;

	@Autowired
	private Environment env;

	@Override
	public void configure(ResourceServerSecurityConfigurer config) {
		config.tokenServices(tokenServices());
		config.resourceId(resourceId);
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(signKey);
		return converter;
	}

	@Bean
	@Primary
	public DefaultTokenServices tokenServices() {
		DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
		defaultTokenServices.setTokenStore(tokenStore());
		return defaultTokenServices;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		if (!Arrays.asList(env.getActiveProfiles()).contains("no-auth")) {
	        http
			.authorizeRequests()
				.antMatchers(HttpMethod.GET, CUSTOMERS).hasAnyRole(RolesEnum.SYSTEM.name(), RolesEnum.CUSTOMER.name())
				.antMatchers(HttpMethod.POST, CUSTOMERS).hasAnyRole(RolesEnum.SYSTEM.name())
				.antMatchers(HttpMethod.PUT, CUSTOMERS).hasAnyRole(RolesEnum.SYSTEM.name())
				.antMatchers(HttpMethod.DELETE, CUSTOMERS).hasAnyRole(RolesEnum.SYSTEM.name())
				.antMatchers(HttpMethod.PATCH, CUSTOMERS).hasAnyRole(RolesEnum.SYSTEM.name())
				.anyRequest().authenticated();
    	} else {
			/**
			 * todas a requisições serão autorizadas caso a autenticacão esteja desativada
			 * usado apenas em ambiente de desenvolvimento e setado no application.properties
			 * spring.profiles.include: no-auth
			 */
    		 http
 			.authorizeRequests()
 				.anyRequest()
 				.permitAll();
    	}
	}

}
