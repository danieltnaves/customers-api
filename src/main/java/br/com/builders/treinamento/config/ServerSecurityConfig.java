package br.com.builders.treinamento.config;

import br.com.builders.treinamento.enumeration.RolesEnum;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Classe responsável em configura a fonte na qual os usuários serão recuperadas e liberar alguns endpoints de acesso.
 */
@Configuration
@EnableWebSecurity
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Método usado para configurar alguns usuários base na memória. Nesse pontos o usuários podem ser recuperados
     * de uma base de dados por exemplo.
     *
     * @param auth
     * @throws Exception
     */
  	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("CUSTOMER_BUILDER").password("123").roles(RolesEnum.CUSTOMER.name());
        auth.inMemoryAuthentication().withUser("SYSTEM_BUILDER").password("123").roles(RolesEnum.SYSTEM.name());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
    @Override
  	public void configure(WebSecurity web) throws Exception {
      	web.ignoring().antMatchers(
                "/v2/api-docs", 
                "/configuration/ui", 
                "/swagger-resources", 
                "/swagger-resources/configuration/ui", 
                "/configuration/security", 
                "/swagger-ui.html", 
                "/webjars/**");
      }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll();
    	//usando para configurar a segurança da URLs quando usado o Spring MVC via WEB
    }
    
}
