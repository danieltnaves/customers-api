/*
* Copyright 2018 Builders
*************************************************************
*Nome     : SwaggerConfig.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.config;

import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

	@Value("${swagger.api.title}")
	private String title;

	@Value("${swagger.api.description}")
	private String description;

	@Value("${swagger.api.version}")
	private String version;

	@Value("${swagger.api.terms}")
	private String terms;

	@Value("${swagger.api.developer.name}")
	private String name;

	@Value("${swagger.api.developer.url}")
	private String url;

	@Value("${swagger.api.developer.email}")
	private String email;

	@Value("${swagger.api.license}")
	private String license;

	@Value("${swagger.api.licenseUrl}")
	private String licenseUrl;

	@Value("${security.oauth2.resource.client.id}")
	private String clientId;

	@Value("${security.oauth2.resource.client.secret}")
	private String clientSecret;

	@Value("${server.port}")
	private String serverPort;

	@Value("${server.contextPath}")
	private String contextPath;

	@Value("${server.hostname}")
	private String hostName;

	@Bean
	public Docket api() {

		List<ResponseMessage> list = new java.util.ArrayList<>();
		list.add(new ResponseMessageBuilder().code(401).message("Unauthorized")
				.responseModel(new ModelRef("Result")).build());
		list.add(new ResponseMessageBuilder().code(403).message("Forbidden")
				.responseModel(new ModelRef("Result")).build());

		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.builders.treinamento"))
				.paths(PathSelectors.any())
				.build()
				.securitySchemes(Collections.singletonList(securitySchema()))
				.securityContexts(Collections.singletonList(securityContext())).pathMapping("/")
				.useDefaultResponseMessages(false).apiInfo(apiInfo())
				.globalResponseMessage(RequestMethod.PUT, list)
				.globalResponseMessage(RequestMethod.POST, list)
				.globalResponseMessage(RequestMethod.GET, list)
				.globalResponseMessage(RequestMethod.DELETE, list)
				.globalResponseMessage(RequestMethod.PATCH, list)
				.directModelSubstitute(DateTime.class, String.class);
	}

	private OAuth securitySchema() {
		List<AuthorizationScope> authorizationScopeList = Lists.newArrayList();
		authorizationScopeList.add(new AuthorizationScope("read", "read client"));
		authorizationScopeList.add(new AuthorizationScope("write", "write client"));
		List<GrantType> grantTypes = Lists.newArrayList();
		GrantType creGrant = new ResourceOwnerPasswordCredentialsGrant(hostName + ":" + serverPort  + contextPath + "/oauth/token");
		grantTypes.add(creGrant);
		return new OAuth("oauth2schema", authorizationScopeList, grantTypes);
	}


	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(systemAuth()).forPaths(PathSelectors.ant("/customers/**"))
				.build();
	}

	private List<SecurityReference> systemAuth() {
		final AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
		authorizationScopes[0] = new AuthorizationScope("read", "read all");
		authorizationScopes[1] = new AuthorizationScope("write", "write all");
		return Collections.singletonList(new SecurityReference("oauth2schema", authorizationScopes));
	}

	@Bean
	public SecurityConfiguration securityInfo() {
		return new SecurityConfiguration(clientId, clientSecret, "", "", "", ApiKeyVehicle.HEADER, "", " ");
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(title).description(description)
				.termsOfServiceUrl(terms)
				.contact(new Contact(name, url, email))
				.license(license).licenseUrl(licenseUrl).version(version).build();
	}
}
