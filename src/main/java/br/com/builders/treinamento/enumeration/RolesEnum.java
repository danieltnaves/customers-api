package br.com.builders.treinamento.enumeration;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

import static java.lang.String.format;

public enum RolesEnum {
  CUSTOMER("CUSTOMER"), SYSTEM("SYSTEM");

  private String code;

  RolesEnum(String code) {
        this.code = code;
    }

  public String getCode() {
    return this.code;
  }

  public static RolesEnum get(String code) {
    return Arrays.stream(RolesEnum.values()).filter(e -> StringUtils.equals(e.code, code)).findFirst()
        .orElseThrow(() -> new IllegalStateException(format("Unsupported code %s.", code)));
  }
}
