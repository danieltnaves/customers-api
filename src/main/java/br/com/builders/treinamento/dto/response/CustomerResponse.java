package br.com.builders.treinamento.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value = "CustomerResponse")
public class CustomerResponse {

  @Id
  @ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world.", position = 1)
  private String id;

  @ApiModelProperty(example = "C645235", value = "Customer ID in the CRM.", position = 2)
  private String crmId;

  @ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container.", position = 3)
  private String baseUrl;

  @ApiModelProperty(example = "Platform Builders", value = "Customer name.", position = 4)
  private String name;

  @ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login.", position = 5)
  private String login;
}
