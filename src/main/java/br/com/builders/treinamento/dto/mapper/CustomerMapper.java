package br.com.builders.treinamento.dto.mapper;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
/**
 * Interface responsável em realizar o mapeamento de DTO para as entidades do modelo. Caso os campos do DTO tenham os
 * mesmos nomes dos campos da entidade de destino o mapstruct conseguirá fazer o mapeamento de forma automática, se
 * os campos possuir nomes diferentes a configuração deverá ser feita de forma explícita utilizando anotações.
 */
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    /**
     * Mapeia um CustomerRequest para um Customer.
     *
     * @param customerRequest dados do customer
     * @return customer mapeado
     */
    Customer customerRequestToCustomer(CustomerRequest customerRequest);

    /**
     * Mapeia um Customer para um CustomerResponse.
     *
     * @param customer dados do customer
     * @return customer response mapeado
     */
    CustomerResponse customerToCustomerResponse(Customer customer);

    /**
     * Mapeia uma lista de Customer para uma lista de CustomerResponse
     *
     */
    List<CustomerResponse> customersToCustomersResponse(List<Customer> customers);

}
