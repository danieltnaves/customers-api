package br.com.builders.treinamento.dto.request;

import br.com.builders.treinamento.dto.groups.ModifyCustomerGroup;
import br.com.builders.treinamento.dto.groups.NewCustomerGroup;
import br.com.builders.treinamento.dto.groups.ReplaceCustomerGroup;
import br.com.builders.treinamento.exception.ErrorCodes;
import br.com.builders.treinamento.utils.ValidationConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Customer")
public class CustomerRequest {

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED,
            groups = {ReplaceCustomerGroup.class})
    @ApiModelProperty(example = "553fa88c-4511-445c-b33a-ddff58d76886", value = "Internal Customer ID, uniquely identifying this customer in the world.", position = 1)
    private String id;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED,
            groups = {NewCustomerGroup.class, ReplaceCustomerGroup.class})
    @Size(message = ErrorCodes.VALIDATE_FIELD_INVALID_RANGE, min = 1, max = 50,
            groups = {NewCustomerGroup.class, ModifyCustomerGroup.class, ReplaceCustomerGroup.class})
    @ApiModelProperty(example = "C645235", value = "Customer ID in the CRM.", position = 2)
    private String crmId;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED,
            groups = {ReplaceCustomerGroup.class})
    @ApiModelProperty(example = "http://www.platformbuilders.com.br", value = "Base URL of the customer container.", position = 3)
    private String baseUrl;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED,
            groups = {NewCustomerGroup.class, ReplaceCustomerGroup.class})
    @Size(message = ErrorCodes.VALIDATE_FIELD_INVALID_RANGE, min = 1, max = 100,
            groups = {NewCustomerGroup.class, ModifyCustomerGroup.class, ReplaceCustomerGroup.class})
    @ApiModelProperty(example = "Platform Builders", value = "Customer name.", position = 4)
    private String name;

    @NotNull(message = ErrorCodes.REQUIRED_FIELD_NOT_INFORMED, groups = {NewCustomerGroup.class, ReplaceCustomerGroup.class})
    @Size(message = ErrorCodes.VALIDATE_FIELD_INVALID_RANGE, min = 1, max = 300,
            groups = {NewCustomerGroup.class, ModifyCustomerGroup.class, ReplaceCustomerGroup.class})
    @Pattern(message = ErrorCodes.VALIDATE_FIELD_INVALID_PATTERN, regexp = ValidationConstants.PATTERN_VALIDATION_EMAIL,
            groups = {NewCustomerGroup.class, ModifyCustomerGroup.class, ReplaceCustomerGroup.class})
    @ApiModelProperty(example = "contato@platformbuilders.com.br", value = "Admin login.", position = 5)
    private String login;

}
